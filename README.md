# Dockerfiles

Here is my Dockerfiles!

Every Dockerfile files have arguments with default value, feel free to explore them to see what they are and what are the possible values.  The allowed values are listed in a manifest file stored just next the Dockerfile file.  If there is no manifest file, that probably means there is no argument possible  Because of the default values, it is possible to build images without passing any argument at all.

Those images are made to support bulk build.  Meaning building a lot of different version of the same image.  Again, see the manifest file stored next to each dockerfile to know the possible version to be built.

Existing Dockerfiles:
 * GnuPG: images containing GnuPG binaries and libraries
 * GnuPG base: images containing GnuPG libraries only

## How to build ?

Go into any directory where there is a Dockerfile.

### Build without argument
```bash
docker build -t test .
```

### Build with argument
```bash
docker build -t test --build-arg MY_ARG=my_value .
```

### Bulk build

I've made a bash script to bulk build called [docker_build](https://gitlab.com/djoproject/config/-/blob/master/docker/docker_build)

Be aware it also tries to push the created images on [hub.docker.com](https://hub.docker.com)

Building every versions:
```bash
docker_build gnupg
```

Building a specifig version:
```bash
docker_build gnupg 2.4.0
```
