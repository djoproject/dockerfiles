ARG BASE_IMAGE_VERSION=3.17

# Libraries

## Building libraries: configure + make + make install

FROM alpine:$BASE_IMAGE_VERSION as building_libs
ARG LIBGPG_ERROR_VERSION=1.46
ARG LIBGCRYPT_VERSION=1.10.1
ARG LIBASSUAN_VERSION=2.5.5
ARG LIBKSBA_VERSION=1.6.3
ARG NPTH_VERSION=1.6
WORKDIR /
COPY install.sh . 
# hadolint ignore=DL3018
RUN chmod +x install.sh \
 && apk add --no-cache gcc gcc-objc make \
 && ./install.sh "$LIBGPG_ERROR_VERSION" "$LIBGCRYPT_VERSION" "$LIBASSUAN_VERSION" "$LIBKSBA_VERSION" "$NPTH_VERSION"

## Libraries without headers

FROM alpine:$BASE_IMAGE_VERSION as libs_with_header
COPY --from=building_libs /usr/local/bin/ /usr/local/bin/
COPY --from=building_libs /usr/local/lib/ /usr/local/lib/
COPY --from=building_libs /usr/local/include/ /usr/local/include/

FROM alpine:$BASE_IMAGE_VERSION as final
COPY --from=libs_with_header /usr/local/ /usr/local/
