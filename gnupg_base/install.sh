#!/usr/bin/env sh

# Set-up 
set -e
mkdir /root/build_gpg
cd /root/build_gpg

# Install libgpg-error
libgpg_error_version="$1"
wget "https://gnupg.org/ftp/gcrypt/libgpg-error/libgpg-error-${libgpg_error_version}.tar.bz2"
tar -xf "libgpg-error-${libgpg_error_version}.tar.bz2"
cd "libgpg-error-${libgpg_error_version}"
./configure --disable-dependency-tracking --enable-install-gpg-error-config
make
make install
cd ..

# Install libgcrypt
libgcrypt_version="$2"
wget "https://gnupg.org/ftp/gcrypt/libgcrypt/libgcrypt-${libgcrypt_version}.tar.bz2"
tar -xf "libgcrypt-${libgcrypt_version}.tar.bz2"
cd "libgcrypt-${libgcrypt_version}"
./configure
make
make install
cd ..

# Install libassuan
libassuan_version="$3"
wget "https://gnupg.org/ftp/gcrypt/libassuan/libassuan-${libassuan_version}.tar.bz2"
tar -xf "libassuan-${libassuan_version}.tar.bz2"
cd "libassuan-${libassuan_version}"
./configure
make
make install
cd ..

# Install libksba
libksba_version="$4"
wget "https://gnupg.org/ftp/gcrypt/libksba/libksba-${libksba_version}.tar.bz2"
tar -xf "libksba-${libksba_version}.tar.bz2"
cd "libksba-${libksba_version}"
./configure
make
make install
cd ..

# Install npth
npth_version="$5"
wget "https://gnupg.org/ftp/gcrypt/npth/npth-${npth_version}.tar.bz2"
tar -xf "npth-${npth_version}.tar.bz2"
cd "npth-${npth_version}"
./configure
make
make install
cd ..
