#!/usr/bin/env sh
set -e

# Recreate ld cache
if ! [ -d "/usr/local/lib" ]; then
    echo "ERROR: no directory /usr/local/lib, is it a gnupg_base image ?" >&2
    exit 1
fi

ldconfig -n /usr/local/lib

# Set-up 
mkdir /root/build_gpg
cd /root/build_gpg

# Installing gpg
gpg_version="$1"
wget "https://gnupg.org/ftp/gcrypt/gnupg/gnupg-${gpg_version}.tar.bz2"
tar -xf "gnupg-${gpg_version}.tar.bz2"
cd "gnupg-${gpg_version}"
./configure

# there is a missing pre-compiler if/endif section for version 2.2.28 when
# ldap support is disabled.
if [ "$gpg_version" = "2.2.28" ]; then
    apk add --no-cache patch
    patch -p0 --ignore-whitespace <<EOF
--- dirmngr/dirmngr.c
+++ dirmngr/dirmngr_ldap.c
@@ -735,6 +735,7 @@ parse_rereadable_options (ARGPARSE_ARGS *pargs, int reread)
     case oStandardResolver: enable_standard_resolver (1); break;
     case oRecursiveResolver: enable_recursive_resolver (1); break;
 
+#if USE_LDAP
     case oLDAPServer:
       {
         ldap_server_t server;
@@ -758,7 +759,7 @@ parse_rereadable_options (ARGPARSE_ARGS *pargs, int reread)
           }
       }
       break;
-
+#endif /*USE_LDAP*/
     case oKeyServer:
       if (*pargs->r.ret_str)
         add_to_strlist (&opt.keyserver, pargs->r.ret_str);
EOF
# there is a missing pre-compiler if/endif section for version 2.2.40 when
# ldap support is disabled.
elif [ "$gpg_version" = "2.2.40" ]; then
    apk add --no-cache patch
    patch -p0 --ignore-whitespace <<EOF
index 651f67cf8..87a0d7784 100644
--- dirmngr/server.c
+++ dirmngr/server_ldap.c
@@ -3135,8 +3135,10 @@ start_command_handler (assuan_fd_t fd, unsigned int session_id)
                ctrl->refcount);
   else
     {
+#if USE_LDAP
       ks_ldap_free_state (ctrl->ks_get_state);
       ctrl->ks_get_state = NULL;
+#endif
       release_ctrl_ocsp_certs (ctrl);
       xfree (ctrl->server_local);
       dirmngr_deinit_default_ctrl (ctrl);
EOF
fi

make
make install

# create an alias gpg to gpg2
if ! [ -f "/usr/local/bin/gpg" ] && [ -f "/usr/local/bin/gpg2" ]; then
    ln -s /usr/local/bin/gpg2 /usr/local/bin/gpg
fi
